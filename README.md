# CodeSample

Code sample for AltSource

## Prompt

> You have been tasked with writing the world’s greatest banking ledger. Please code a solution that can perform the following workflows through a console application (accessed via the command line):
>
> * Create a new account
> * Login
> * Record a deposit
> * Record a withdrawal
> * Check balance
> * See transaction history
> * Log out
> 
> For additional credit, you may implement this through a web page. They don’t have to run at the same time, but if you would like to do that, feel free.
> 
> Use C# for the backend. If designing a front end, use whatever JavaScript frameworks/libraries you wish and just make sure they are included or available via NuGet. Please don't use persistent storage (e.g. SQL database) or spend much time on the UI (unless you love doing that).

## What's included

Projects in the solution:

### Ledger

The requested features are implemented here.

I used an in-memory SQLite database to get as close as I could to what I would do if I actually had persistent storage available.

You'll notice I did not actually implement any security on the accounts... my general rule here is: Unless your business is creating security software, don't write your own. In a real setting, I'd reach first for whatever platform-integrated security and account management is available, and if I had to manage it myself, I'd likely go with storing salted password hashes.

Cut for time: Better error handling and sanity checking.

### LedgerTests

Unit tests for the Ledger project. Not requested, but I included them because I believe testing is important.

Cut for time: Mocks for the data context, or at least its collection properties.

### ConsoleDemo

UI for the requested features.

If this seems overly complex, it's meant to be illustrative. In a situation where I'd be regularly producing console tools, I'd want to come up with an extensible framework to handle the repeated tasks (*e.g.*, selecting options, getting input), leaving only application-specific code to be written; this is a semi-generic first thought in that direction.

### LedgerService

A start on a more Web-centric implementation.

Same notes on security as above.

Normally I'd never leave an open data connection in a Web application's cache like I did here; it's like this only because the SQLite in-memory database destroys everything when the connection is closed. In a real solution, we'd have persistent storage available and each data operation would create and dispose of its own connection.

If you run this from a browser, be aware that Chrome consistently called each method *twice* for me, while Edge did not.

## Not included

I left these out as I've hit the requirements and I'd rather turn in something sooner, given schedule constraints:

### Web application

This would (more or less) call the ledger service and display the results.

### Simultaneous console/Web

Straightforward enough; simply modify the console application to call into the ledger service instead of using the core library directly. The UI portion of the console app would not need to change.
