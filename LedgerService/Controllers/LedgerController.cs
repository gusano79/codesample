﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;

namespace LedgerService.Controllers
{
    [Route("{action}")]
    [ApiController]
    public class LedgerController : ControllerBase
    {
        private readonly IMemoryCache Cache;

        private Ledger.LedgerController Ledger
        {
            get => Cache.GetOrCreate(
                "Ledger",
                entry =>
                {
                    entry.Priority = CacheItemPriority.NeverRemove;

                    var connection = new SqliteConnection("DataSource=:memory:");
                    connection.Open();

                    var context = new Ledger.Data.LedgerContext(
                        new DbContextOptionsBuilder<Ledger.Data.LedgerContext>()
                            .UseSqlite(connection)
                            .Options
                    );

                    context.Database.EnsureCreated();

                    return new Ledger.LedgerController(context);
                }
            );
        }

        public LedgerController(IMemoryCache cache)
        {
            Cache = cache;
        }

        [HttpGet]
        public int CreateAccount(string name)
        {
            return Ledger.CreateAccount(name);
        }

        [HttpGet]
        public int Login(string accountName)
        {
            return Ledger.Login(accountName);
        }

        [HttpGet]
        public void Deposit(int accountID, decimal amount)
        {
            Ledger.Deposit(accountID, amount);
        }

        [HttpGet]
        public void Withdraw(int accountID, decimal amount)
        {
            Ledger.Withdraw(accountID, amount);
        }

        [HttpGet]
        public decimal GetBalance(int accountID)
        {
            return Ledger.GetBalance(accountID);
        }

        [HttpGet]
        public IEnumerable<Ledger.LedgerHistoryRow> GetHistory(int accountID)
        {
            return Ledger.GetHistory(accountID);
        }
    }
}
