﻿using Ledger.Data;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Linq;

namespace LedgerTests.Data
{
    [TestFixture]
    public class LedgerContextTests
    {
        [Test]
        public void NewContextIsEmpty()
        {
            LedgerContext.WithInMemoryData(context =>
            {
                Assert.That(
                    context.Accounts.Count(),
                    Is.EqualTo(0)
                );

                Assert.That(
                    context.Entries.Count(),
                    Is.EqualTo(0)
                );
            });
        }

        [Test]
        public void AddNewAccountSucceeds()
        {
            LedgerContext.WithInMemoryData(context =>
            {
                context.Accounts.Add(new Account
                {
                    Name = "testacct"
                });
                context.SaveChanges();

                Assert.That(
                    context.Accounts.Count(),
                    Is.EqualTo(1)
                );

                Assert.That(
                    context.Accounts.First().Name,
                    Is.EqualTo("testacct")
                );
            });
        }

        [Test]
        public void AddNewEntryWithNoAccountFails()
        {
            LedgerContext.WithInMemoryData(context =>
            {
                context.Entries.Add(new Entry
                {
                    Amount = 1,
                    Timestamp = DateTime.UtcNow
                });
                
                Assert.Throws<DbUpdateException>(
                    () => context.SaveChanges()
                );
            });
        }

        [Test]
        public void AddNewEntryToAccountSucceeds()
        {
            LedgerContext.WithInMemoryData(context =>
            {
                var account = new Account
                {
                    Name = "testacct"
                };

                context.Accounts.Add(account);
                context.SaveChanges();

                context.Entries.Add(new Entry
                {
                    Account = account
                });
                context.SaveChanges();

                Assert.That(
                    context.Entries.Count(),
                    Is.EqualTo(1)
                );

                var entry = context.Entries.First();

                Assert.That(
                    entry.AccountID,
                    Is.EqualTo(account.ID)
                );
            });
        }

        [Test]
        public void AddNewEntryHasEnoughPrecision()
        {
            LedgerContext.WithInMemoryData(context =>
            {
                var account = new Account
                {
                    Name = "testacct"
                };

                context.Accounts.Add(account);
                context.SaveChanges();

                context.Entries.Add(new Entry
                {
                    Account = account,
                    Amount = 1.23m
                });
                context.SaveChanges();

                Assert.That(
                    context.Entries.First().Amount,
                    Is.EqualTo(1.23m)
                );
            });
        }

        [Test]
        public void AccountNameMustBeUnique()
        {
            LedgerContext.WithInMemoryData(context =>
            {
                var account = new Account
                {
                    Name = "testacct"
                };

                context.Accounts.Add(account);
                context.SaveChanges();

                Assert.Throws<DbUpdateException>(() =>
                {
                    context.Accounts.Add(new Account
                    {
                        Name = "testacct"
                    });
                    context.SaveChanges();
                });
            });
        }
    }
}
