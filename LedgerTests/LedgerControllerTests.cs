﻿using Ledger;
using Ledger.Data;
using NUnit.Framework;
using System;
using System.Linq;

namespace LedgerTests
{
    [TestFixture]
    public class LedgerControllerTests
    {
        private void WithInMemoryController(Action<LedgerContext, LedgerController> action)
        {
            LedgerContext.WithInMemoryData(context =>
            {
                action(
                    context,
                    new LedgerController(context)
                );
            });
        }

        [Test]
        public void CreateAccount()
        {
            WithInMemoryController((context, controller) =>
            {
                controller.CreateAccount("flibble");

                Assert.That(
                    context.Accounts.Single().Name,
                    Is.EqualTo("flibble")
                );
            });
        }

        [Test]
        public void CreateDuplicateAccountFails()
        {
            WithInMemoryController((context, controller) =>
            {
                controller.CreateAccount("womprat");

                Assert.Throws<LedgerException>(
                    () => controller.CreateAccount("womprat")
                );
            });
        }

        [Test]
        public void Login()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("biggles");

                Assert.That(
                    controller.Login("biggles"),
                    Is.EqualTo(accountID)
                );
            });
        }

        [Test]
        public void LoginMissingAccountFails()
        {
            WithInMemoryController((context, controller) =>
            {
                Assert.Throws<LedgerException>(
                    () => controller.Login("winkie")
                );
            });
        }

        [Test]
        public void Deposit()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("huey");
                controller.Deposit(accountID, 123.45m);

                Assert.That(
                    context.Entries.Single().Amount,
                    Is.EqualTo(123.45m)
                );
            });
        }

        [Test]
        public void DepositNegativeFails()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("dewey");

                Assert.Throws<LedgerException>(
                    () => controller.Deposit(accountID, - 2m)
                );
            });
        }

        [Test]
        public void DepositMissingAccountFails()
        {
            WithInMemoryController((context, controller) =>
            {
                Assert.Throws<LedgerException>(
                    () => controller.Deposit(-666, 10m)
                );
            });
        }

        [Test]
        public void Withdraw()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("louie");
                controller.Deposit(accountID, 1000m);

                controller.Withdraw(accountID, 543.21m);

                Assert.That(
                    context.Entries
                        .OrderBy(e => e.ReferenceID)
                        .Last()
                        .Amount,
                    Is.EqualTo(-543.21m)
                );
            });
        }

        [Test]
        public void WithdrawNegativeFails()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("agamemnon");

                Assert.Throws<LedgerException>(
                    () => controller.Withdraw(accountID, - 101m)
                );
            });
        }

        [Test]
        public void WithdrawTooMuchFails()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("greedy");

                Assert.Throws<LedgerException>(
                    () => controller.Withdraw(accountID, 1000m)
                );
            });
        }

        [Test]
        public void WithdrawMissingAccountFails()
        {
            WithInMemoryController((context, controller) =>
            {
                Assert.Throws<LedgerException>(
                    () => controller.Withdraw(-666, 10m)
                );
            });
        }

        [Test]
        public void ReferenceIDIncrementsFromOne()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("refidtest");

                controller.Deposit(accountID, 1m);
                controller.Deposit(accountID, 2m);
                controller.Deposit(accountID, 4m);
                controller.Deposit(accountID, 8m);

                Assert.That(
                    context.Entries.Single(e => e.Amount == 1m).ReferenceID,
                    Is.EqualTo(1)
                );

                Assert.That(
                    context.Entries.Single(e => e.Amount == 2m).ReferenceID,
                    Is.EqualTo(2)
                );

                Assert.That(
                    context.Entries.Single(e => e.Amount == 4m).ReferenceID,
                    Is.EqualTo(3)
                );

                Assert.That(
                    context.Entries.Single(e => e.Amount == 8m).ReferenceID,
                    Is.EqualTo(4)
                );
            });
        }

        [Test]
        public void ReferenceIDsForMultipleAccountsIncrementIndependently()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID1 = controller.CreateAccount("multiref-1");
                controller.Deposit(accountID1, 3m);
                controller.Deposit(accountID1, 11m);

                var accountID2 = controller.CreateAccount("multiref-2");
                controller.Deposit(accountID2, 54m);

                controller.Deposit(accountID1, 123m);

                Assert.That(
                    context.Entries.Single(e => e.Amount == 3m).ReferenceID,
                    Is.EqualTo(1)
                );

                Assert.That(
                    context.Entries.Single(e => e.Amount == 11m).ReferenceID,
                    Is.EqualTo(2)
                );

                Assert.That(
                    context.Entries.Single(e => e.Amount == 54m).ReferenceID,
                    Is.EqualTo(1)
                );

                Assert.That(
                    context.Entries.Single(e => e.Amount == 123m).ReferenceID,
                    Is.EqualTo(3)
                );
            });
        }

        [Test]
        public void GetBalance()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("balancetest");
                controller.Deposit(accountID, 100m);
                controller.Withdraw(accountID, 12.95m);
                controller.Deposit(accountID, 10m);

                Assert.That(
                    controller.GetBalance(accountID),
                    Is.EqualTo(97.05m)
                );
            });
        }

        [Test]
        public void GetBalanceMissingAccountFails()
        {
            WithInMemoryController((context, controller) =>
            {
                Assert.Throws<LedgerException>(
                    () => controller.GetBalance(-666)
                );
            });
        }

        [Test]
        public void GetHistory()
        {
            WithInMemoryController((context, controller) =>
            {
                var noiseID = controller.CreateAccount("noise");
                controller.Deposit(noiseID, 12345m);
                controller.Withdraw(noiseID, 2357.11m);

                var accountID = controller.CreateAccount("historytest");
                controller.Deposit(accountID, 1000m);
                controller.Withdraw(accountID, 99.95m);
                controller.Withdraw(accountID, 33.78m);
                controller.Deposit(accountID, 101m);

                var history = controller.GetHistory(accountID).ToList();

                Assert.That(history.Count, Is.EqualTo(4));

                Assert.That(history[0].ReferenceID, Is.EqualTo(1));
                Assert.That(history[0].Type, Is.EqualTo(LedgerHistoryRowType.Deposit));
                Assert.That(history[0].Amount, Is.EqualTo(1000m));
                Assert.That(history[0].Balance, Is.EqualTo(1000m));

                Assert.That(history[1].ReferenceID, Is.EqualTo(2));
                Assert.That(history[1].Type, Is.EqualTo(LedgerHistoryRowType.Withdrawal));
                Assert.That(history[1].Amount, Is.EqualTo(99.95m));
                Assert.That(history[1].Balance, Is.EqualTo(900.05m));

                Assert.That(history[2].ReferenceID, Is.EqualTo(3));
                Assert.That(history[2].Type, Is.EqualTo(LedgerHistoryRowType.Withdrawal));
                Assert.That(history[2].Amount, Is.EqualTo(33.78m));
                Assert.That(history[2].Balance, Is.EqualTo(866.27m));

                Assert.That(history[3].ReferenceID, Is.EqualTo(4));
                Assert.That(history[3].Type, Is.EqualTo(LedgerHistoryRowType.Deposit));
                Assert.That(history[3].Amount, Is.EqualTo(101m));
                Assert.That(history[3].Balance, Is.EqualTo(967.27m));
            });
        }

        [Test]
        public void GetHistoryNoEntriesIsEmpty()
        {
            WithInMemoryController((context, controller) =>
            {
                var accountID = controller.CreateAccount("historytest");

                CollectionAssert.IsEmpty(
                    controller.GetHistory(accountID)
                );
                
            });
        }

        [Test]
        public void GetHistoryMissingAccountFails()
        {
            WithInMemoryController((context, controller) =>
            {
                Assert.Throws<LedgerException>(
                    () => controller.GetHistory(-666)
                );
            });
        }
    }
}
