﻿using System;

namespace Ledger.Data
{
    /// <summary>
    /// A transaction entry in a ledger
    /// </summary>
    public class Entry
    {
        /// <summary>
        /// The entry's unique identifier
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The unique identifier of the <see cref="Account"/> to which this entry belongs
        /// </summary>
        public int AccountID { get; set; }

        /// <summary>
        /// The <see cref="Account"/> to which this entry belongs
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// A reference identifier for the entry
        /// </summary>
        /// <remarks>
        /// Only unique within its associated account; can be used
        /// for sorting when date stamps are not precise enough.
        /// </remarks>
        public int ReferenceID { get; set; }

        /// <summary>
        /// The date and time of the transaction
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// The amount of the transaction
        /// </summary>
        /// <remarks>
        /// Deposits have positive amounts, withdrawals
        /// are negative.
        /// </remarks>
        public decimal Amount { get; set; }
    }
}
