﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;

namespace Ledger.Data
{
    /// <summary>
    /// Data context for a simple ledger database.
    /// </summary>
    public class LedgerContext : DbContext
    {
        /// <summary>
        /// Handles creating and disposing of a <see cref="LedgerContext"/>
        /// that uses an in-memory SQLite database.
        /// </summary>
        /// <param name="action">The action to perform with the data context</param>
        public static void WithInMemoryData(Action<LedgerContext> action)
        {
            using var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            using var context = new LedgerContext(
                new DbContextOptionsBuilder<LedgerContext>()
                    .UseSqlite(connection)
                    .Options
            );

            context.Database.EnsureCreated();

            action(context);
        }

        public LedgerContext(DbContextOptions<LedgerContext> options) :
            base(options)
        {
        }

        /// <summary>
        /// Accounts tracked by the ledger.
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Entries recored in the ledger, by account.
        /// </summary>
        public DbSet<Entry> Entries { get; set; }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<Account>().HasKey(a => a.ID);
            mb.Entity<Account>().Property(a => a.Name)
                .IsRequired();
            mb.Entity<Account>().HasIndex(a => a.Name)
                .IsUnique();
            mb.Entity<Account>().HasMany(a => a.Entries)
                .WithOne(e => e.Account)
                .HasForeignKey(e => e.AccountID);

            mb.Entity<Entry>().HasKey(e => e.ID);
            mb.Entity<Entry>().Property(e => e.Timestamp)
                .IsRequired();
            mb.Entity<Entry>().Property(e => e.Amount)
                .IsRequired();
        }
    }
}
