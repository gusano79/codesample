﻿using System.Collections.Generic;

namespace Ledger.Data
{
    /// <summary>
    /// An account tracked by a ledger.
    /// </summary>
    public class Account
    {
        /// <summary>
        /// The account's unique identifier
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The account's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Transaction records associated with this account
        /// </summary>
        public ICollection<Entry> Entries { get; set; }
    }
}
