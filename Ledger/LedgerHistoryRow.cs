﻿using Ledger.Data;
using System;

namespace Ledger
{
    /// <summary>
    /// A row of single-entry bookkeeping data
    /// </summary>
    /// <remarks>
    /// Meant for reporting purposes.
    /// </remarks>
    public class LedgerHistoryRow
    {
        /// <summary>
        /// The <see cref="Entry.ReferenceID"/> of the source <see cref="Entry"/>
        /// </summary>
        public int ReferenceID { get; set; }

        /// <summary>
        /// The <see cref="Entry.Timestamp"/> of the source <see cref="Entry"/>
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// The type of transaction
        /// </summary>
        public LedgerHistoryRowType Type { get; set; }

        /// <summary>
        /// The amount of this transaction
        /// </summary>
        /// <remarks>
        /// Should always be positive.
        /// </remarks>
        public decimal Amount { get; set; }

        /// <summary>
        /// The total balance in the account after this transaction
        /// </summary>
        public decimal Balance { get; set; }
    }
}
