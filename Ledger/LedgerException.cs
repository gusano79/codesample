﻿using System;

namespace Ledger
{
    /// <summary>
    /// A Ledger-related exception
    /// </summary>
    public class LedgerException : Exception
    {
        /// <summary>
        /// Creates a new ledger-related exception
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="innerException">
        /// The exception that caused this exception, or <c>null</c> if there isn't one
        /// </param>
        public LedgerException(string message, Exception innerException = null) :
            base(message, innerException)
        {
        }
    }
}
