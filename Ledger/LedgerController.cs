﻿using Ledger.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ledger
{
    /// <summary>
    /// Manages ledger data in a <see cref="LedgerContext"/>
    /// </summary>
    public class LedgerController
    {
        private LedgerContext Context { get; set; }

        /// <summary>
        /// Creates a new ledger controller
        /// </summary>
        /// <param name="context">The data context to manage</param>
        public LedgerController(LedgerContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Creates a new account
        /// </summary>
        /// <param name="name">The name of the account</param>
        /// <returns>
        /// Returns the <see cref="Account.ID">unique identifier</see> of the created account
        /// </returns>
        /// <exception cref="LedgerException">
        /// The account could not be created.
        /// </exception>
        public int CreateAccount(string name)
        {
            if(Context.Accounts.FirstOrDefault(a => a.Name == name) != null)
            {
                throw new LedgerException("Account already exists.");
            }

            try
            {
                var account = new Account
                {
                    Name = name
                };

                Context.Accounts.Add(account);
                Context.SaveChanges();

                return account.ID;
            }
            catch(DbUpdateException ex)
            {
                throw new LedgerException(
                    $"Couldn't create account '{name}'.",
                    ex.InnerException
                );
            }
        }

        /// <summary>
        /// Retrieves an account ID
        /// </summary>
        /// <param name="accountName">The name of the account to look up</param>
        /// <returns>
        /// Returns the <see cref="Account.ID">unique identifier</see> of the created account
        /// </returns>
        /// <exception cref="LedgerException">
        /// The account does not exist.
        /// </exception>
        /// <remarks>
        /// This is just a placeholder for what would normally be some sort of security check.
        /// </remarks>
        public int Login(string accountName)
        {
            var account = Context.Accounts.FirstOrDefault(
                a => a.Name == accountName
            );

            if (account != null) return account.ID;
            else throw new LedgerException("Account doesn't exist.");
        }

        private void CheckAccount(int accountID)
        {
            if (Context.Accounts.FirstOrDefault(a => a.ID == accountID) == null)
            {
                throw new LedgerException("Account doesn't exist.");
            }
        }

        private void AddEntry(int accountID, decimal amount)
        {
            try
            {
                var refID = Context.Entries
                    .Where(e => e.AccountID == accountID)
                    .Select(e => e.ReferenceID)
                    .ToList() // EF couldn't handle DefaultIfEmpty here
                    .DefaultIfEmpty()
                    .Max() + 1;

                Context.Entries.Add(new Entry
                {
                    AccountID = accountID,
                    ReferenceID = refID,
                    Timestamp = DateTime.UtcNow,
                    Amount = amount
                });

                Context.SaveChanges();
            }
            catch(DbUpdateException ex)
            {
                throw new LedgerException(
                    $"Couldn't add a ledger entry.",
                    ex
                );
            }
        }

        /// <summary>
        /// Record a deposit in the ledger
        /// </summary>
        /// <param name="accountID">The account to update</param>
        /// <param name="amount">The amount of the deposit; must be positive</param>
        /// <exception cref="LedgerException">
        /// The record couldn't be created, or the amount was negative.
        /// </exception>
        public void Deposit(int accountID, decimal amount)
        {
            CheckAccount(accountID);

            if (amount == 0) return;
            else if (amount < 0)
            {
                throw new LedgerException(
                    "Can't deposit a negative amount."
                );
            }

            AddEntry(accountID, amount);
        }


        /// <summary>
        /// Record a withdrawal in the ledger
        /// </summary>
        /// <param name="accountID">The account to update</param>
        /// <param name="amount">The amount of the withdrawal; must be positive</param>
        /// <exception cref="LedgerException">
        /// The record couldn't be created, or the amount was negative.
        /// </exception>
        public void Withdraw(int accountID, decimal amount)
        {
            CheckAccount(accountID);

            if (amount == 0) return;
            else if (amount < 0)
            {
                throw new LedgerException(
                    "Can't withdraw a negative amount."
                );
            }
            else if (amount > GetBalance(accountID))
            {
                throw new LedgerException(
                    "Can't withdraw more than the account contains."
                );
            }

            AddEntry(accountID , - amount);
        }

        /// <summary>
        /// Gets an account's current balance
        /// </summary>
        /// <param name="accountID">The account to look up</param>
        /// <returns>
        /// Returns the current balance of the specified account.
        /// </returns>
        /// <remarks>
        /// Calculates the balance based on all ledger entries associated
        /// with the account. This is fast enough for this code sample's purposes,
        /// but might not be for a system expecting many more records. In that case,
        /// I might cache the current balance with the account, and include appropriate
        /// audit/correction methods.
        /// </remarks>
        public decimal GetBalance(int accountID)
        {
            CheckAccount(accountID);

            return Context.Entries
                .Where(e => e.AccountID == accountID)
                .ToList() // EF can't do the aggregation
                .Aggregate(
                    0m,
                    (acc, entry) => acc + entry.Amount
                );
        }

        /// <summary>
        /// Gets the transaction history for an account
        /// </summary>
        /// <param name="accountID">The account to look up</param>
        /// <returns>
        /// Returns a collection of <see cref="LedgerHistoryRow">history records</see>
        /// for the account, sorted in the order they were created.
        /// </returns>
        public IEnumerable<LedgerHistoryRow> GetHistory(int accountID)
        {
            CheckAccount(accountID);

            var runningTotal = 0m;

            return Context.Entries
                .Where(e => e.AccountID == accountID)
                .OrderBy(e => e.ReferenceID)
                .ToList() // EF can't do the running total
                .Select(e => new LedgerHistoryRow
                {
                    ReferenceID = e.ReferenceID,
                    Timestamp = e.Timestamp,
                    Type = e.Amount > 0 ? LedgerHistoryRowType.Deposit : LedgerHistoryRowType.Withdrawal,
                    Amount = e.Amount > 0 ? e.Amount : -e.Amount,
                    Balance = (runningTotal += e.Amount)
                });
        }
    }
}
