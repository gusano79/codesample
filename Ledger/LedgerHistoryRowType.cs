﻿namespace Ledger
{
    /// <summary>
    /// Transaction type
    /// </summary>
    /// <remarks>
    /// Used when reporting transaction history with <see cref="LedgerHistoryRow"/>.
    /// </remarks>
    public enum LedgerHistoryRowType
    {
        /// <summary>
        /// A deposit/credit to the account
        /// </summary>
        Deposit,

        /// <summary>
        /// A withdrawal/debit from the account
        /// </summary>
        Withdrawal
    }
}
