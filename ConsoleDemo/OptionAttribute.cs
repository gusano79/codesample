﻿using System;

namespace ConsoleDemo
{
    class OptionAttribute : Attribute
    {
        public string Description { get; set; }

        public bool UseWhenLoggedIn { get; set; }

        public bool UseWhenLoggedOut { get; set; }

        public OptionAttribute(string description, bool useWhenLoggedIn, bool useWhenLoggedOut)
        {
            Description = description;
            UseWhenLoggedIn = useWhenLoggedIn;
            UseWhenLoggedOut = useWhenLoggedOut;
        }
    }
}
