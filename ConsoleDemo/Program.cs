﻿using Ledger;
using Ledger.Data;
using System;
using System.Linq;
using System.Reflection;

namespace ConsoleDemo
{
    class Program
    {
        static void Main()
        {
            LedgerContext.WithInMemoryData(context =>
            {
                var session = new Session
                {
                    Account = null,
                    Controller = new LedgerController(context)
                };

                Console.WriteLine("Ledger code sample, console interface");
                Console.WriteLine();

                bool running = true;
                do
                {
                    var loggedIn = session.Account != null;

                    Console.WriteLine();
                    Console.WriteLine(
                        loggedIn
                            ? $"Logged in to account '{session.Account.Name}'"
                            : "Not logged in"
                    );
                    Console.WriteLine();
                    Console.WriteLine("Select an option:");

                    try
                    {
                        running = SelectOption(loggedIn)(session);
                    }
                    catch(LedgerException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                while (running);
            });
        }

        class Session
        {
            public Account Account { get; set; }

            public LedgerController Controller { get; set; }
        }

        delegate bool OptionAction(Session session);

        class Option
        {
            public string Description { get; set; }
            public OptionAction Action { get; set; }
        }

        static readonly Option[] LoggedInOptions;
        static readonly Option[] LoggedOutOptions;

        static OptionAction SelectOption(bool loggedIn)
        {
            var options = loggedIn ? LoggedInOptions : LoggedOutOptions;

            for(int i = 0; i < options.Length; i++)
            {
                Console.WriteLine($"{i + 1}. {options[i].Description}");
            }

            Console.WriteLine();
            Console.Write("> ");

            int selected;
            do
            {
                selected = Console.ReadKey(true).KeyChar - '1';
            }
            while (selected < 0 || selected >= options.Length);

            Console.WriteLine(options[selected].Description);
            Console.WriteLine();

            return options[selected].Action;
        }

        [Option("Create a new account", false, true)]
        static bool CreateAccount(Session session)
        {
            Console.WriteLine("Enter a new account name:");
            Console.WriteLine();
            Console.Write("> ");

            var accountName = Console.ReadLine().Trim();
            var accountID = session.Controller.CreateAccount(accountName);

            session.Account = new Account
            {
                Name = accountName,
                ID = accountID
            };

            return true;
        }

        [Option("Log in", false, true)]
        static bool Login(Session session)
        {
            Console.WriteLine("Enter account name: ");
            Console.WriteLine();
            Console.Write("> ");

            var accountName = Console.ReadLine().Trim();
            var accountID = session.Controller.Login(accountName);

            session.Account = new Account
            {
                Name = accountName,
                ID = accountID
            };

            return true;
        }

        static decimal? ReadDecimal()
        {
            return decimal.TryParse(Console.ReadLine(), out decimal value)
                ? value
                : (decimal?)null;
        }

        [Option("Make a deposit", true, false)]
        static bool Deposit(Session session)
        {
            Console.WriteLine("Enter deposit amount:");
            Console.WriteLine();
            Console.Write("> ");

            var amount = ReadDecimal();
            Console.WriteLine();

            if (amount.HasValue)
            {
                session.Controller.Deposit(
                    session.Account.ID,
                    amount.Value
                );

                Console.WriteLine($"Deposited {amount.Value:N2}");
                Console.WriteLine(
                    "New balance is {0:N2}",
                    session.Controller.GetBalance(session.Account.ID)
                );
            }
            else
            {
                Console.WriteLine("That's not a decimal value; no deposit made.");
            }

            return true;
        }

        [Option("Make a withdrawal", true, false)]
        static bool Withdraw(Session session)
        {
            Console.WriteLine("Enter withdrawal amount:");
            Console.WriteLine();
            Console.Write("> ");

            var amount = ReadDecimal();
            Console.WriteLine();

            if (amount.HasValue)
            {
                session.Controller.Withdraw(
                    session.Account.ID,
                    amount.Value
                );

                Console.WriteLine($"Withdrew {amount.Value:N2}");
                Console.WriteLine(
                    "New balance is {0:N2}",
                    session.Controller.GetBalance(session.Account.ID)
                );
            }
            else
            {
                Console.WriteLine("That's not a decimal value; no withdrawal made.");
            }

            return true;
        }

        [Option("Show account balance", true, false)]
        static bool ShowBalance(Session session)
        {
            Console.WriteLine(
                "Account balance: {0:N2}",
                session.Controller.GetBalance(session.Account.ID)
            );

            return true;
        }

        [Option("Show transaction history", true, false)]
        static bool ShowHistory(Session session)
        {
            var history = session.Controller.GetHistory(session.Account.ID).ToList();

            if (history.Count == 0)
            {
                Console.WriteLine("There is no transaction history for this account.");
            }
            else
            {
                Console.WriteLine("ID   Timestamp             Deposit       Withdrawal    Balance");
                Console.WriteLine("--  --------------------  ------------  ------------  ------------");
                foreach (var h in history)
                {
                    Console.WriteLine(
                        "{0,2}  {1,-20:u}  {2,12:N2}  {3,12:N2}  {4,12:N2}",
                        h.ReferenceID,
                        h.Timestamp,
                        h.Type == LedgerHistoryRowType.Deposit
                            ? h.Amount
                            : (decimal?)null,
                        h.Type == LedgerHistoryRowType.Withdrawal
                            ? h.Amount
                            : (decimal?)null,
                        h.Balance
                    );
                }
            }

            return true;
        }

        [Option("Log out", true, false)]
        static bool LogOut(Session session)
        {
            session.Account = null;

            return true;
        }

        [Option("Quit", true, true)]
        static bool Quit(Session session)
        {
            Console.WriteLine("Goodbye...");

            return false;
        }

        static Program()
        {
            var allOptions = typeof(Program)
                .GetMethods(
                    BindingFlags.Static
                    | BindingFlags.NonPublic
                    | BindingFlags.DeclaredOnly
                )
                .Select(mi => new
                {
                    Info = mi,
                    Attribute = mi.GetCustomAttribute<OptionAttribute>() as OptionAttribute
                })
                .Where(x => x.Attribute != null);

            LoggedInOptions = allOptions
                .Where(o => o.Attribute.UseWhenLoggedIn == true)
                .Select(o => new Option
                {
                    Description = o.Attribute.Description,
                    Action = o.Info.CreateDelegate(typeof(OptionAction)) as OptionAction
                })
                .Where(o => o.Action != null)
                .ToArray();

            LoggedOutOptions = allOptions
                .Where(o => o.Attribute.UseWhenLoggedOut == true)
                .Select(o => new Option
                {
                    Description = o.Attribute.Description,
                    Action = o.Info.CreateDelegate(typeof(OptionAction)) as OptionAction
                })
                .Where(o => o.Action != null)
                .ToArray();
        }
    }
}
